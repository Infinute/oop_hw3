
public class POOBoard {
    private String name;
    public POOArticle[] article;
    private boolean[] article_exist;
    private int tail;
    private int length;
    public POOBoard(String name){
	this.name = name;
	article = new POOArticle[1024];
	article_exist = new boolean[1024];
	int i=-1;
	while(++i<1024){
	    article_exist[i] = false;
	}
	tail = 0;
	length = 0;
    }
    public int add(POOArticle article){
	if (tail < 1024){
		article_exist[tail]=true;
		this.article[tail]=article;
		length++;
		tail++;
	} else {
		int i=-1;
		if (length<1024){
			i = 1024;
			while(article_exist[--i]==true)
				continue;
		}
		while(++i<1023){
			article_exist[i]
				= article_exist[i+1];
			this.article[i]
				= this.article[i+1]; 
		}
		this.article[1023] = article;
	}
	return 1;
    }
    public void del(int pos){
	article_exist[pos]=false;
	article[pos]= null;
	length--;
	if (pos == tail-1)
		tail--;
    }
    public void move(int src, int dest){
	article[dest]=article[src];
	article[src]= null;
	article_exist[src] = false;
	article_exist[dest] = true;
	if (dest>=tail)
		tail=dest+1;
	//move the article at position src to position dest
    }
    public int length(){
	return length;
    }
    public void show(){
	int i=-1;
	while(++i<1024){
		if (article_exist[i]==true)
			System.out.printf("%3d   %s\n",i,article[i].title());
	}
	//show the article titles of the board
    }
    public String name(){
    	return name;
    }
}
