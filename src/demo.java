import java.util.Scanner;

public class demo {
    public static void main(String[] argc){
	System.out.println("System: POOArticle & POOBoard demo start");
	Scanner scanner = new Scanner(System.in);
	System.out.print("System: Create a board named OOP...");
	POOBoard oopboard = new POOBoard("OOP");
	System.out.print("Press Enter to continue");  
	scanner.nextLine();
	int ID = 0;
	System.out.println("System: Write an article on board OOP...");
	System.out.print("System: Please enter the title:");
	String title = scanner.nextLine();
	System.out.print("System: Please enter the author's name:");	
	String author = scanner.nextLine();
        System.out.println("System: please enter the content. To quit, please type exit:");

        String content = new String();
        while (true)
        {
            String str = scanner.nextLine();
            if (str.equals("exit"))
                break;
            content = content + str + "\n";
        }
        POOArticle temp_article = new POOArticle(ID, title, author, content);
        ID += oopboard.add(temp_article);
        System.out.println("System: automatically adding 9 testing Evaluation messages...");   
        oopboard.article[0].push("push!");
        oopboard.article[0].push("push!");
        oopboard.article[0].boo("boo!");
        oopboard.article[0].boo("boo!");
        oopboard.article[0].arrow("arrow!");
        oopboard.article[0].arrow("arrow!");
        oopboard.article[0].push("push!");
        oopboard.article[0].push("push!");
        oopboard.article[0].push("push!");  
        System.out.print("System: Press Enter to list() the article");   
	scanner.nextLine();
        oopboard.article[0].list();     
        System.out.println("System: Press Enter to show() the article");   
	scanner.nextLine();    
        oopboard.article[0].show();     
	System.out.print("System: Press Enter to continue");  
	scanner.nextLine();  
        
        System.out.println("System: automatically adding 9 testing articles...");
        for (int i=1; i<10; i++){
            title = "TEST " + i;
            author = "System";
            content = "testing " + i;
            temp_article = new POOArticle(ID, title, author, content);
            ID += oopboard.add(temp_article);
        }	 
        System.out.print("System: Press Enter to show() the board");   
	scanner.nextLine();  
        oopboard.show();     
	System.out.print("System: Press Enter to continue");  
	scanner.nextLine();
	
        System.out.println("System: move(1,11) the board");   	
        oopboard.move(1,11);     
        System.out.println("System: Press Enter to show() the board");   
	scanner.nextLine();
        oopboard.show();     
	System.out.print("System: Press Enter to continue");  
	scanner.nextLine();	
	
        System.out.println("System: del(5) the board");   	
        oopboard.del(5);     
        System.out.print("System: Press Enter to show() the board");
	scanner.nextLine();
        oopboard.show();
        System.out.print("System: Press Enter to output the length of the board");  
	scanner.nextLine();
	System.out.printf("the length is : %d\n", oopboard.length());
	System.out.print("System: POOArticle & POOBoard demo end. Press Enter to continue");  
	scanner.nextLine();
	
	//POODirectory demo
	System.out.println("System: POODirectory demo start.");  
//	System.out.print("System: Please enter the POODirectory's name:");
//	String name = scanner.nextLine();	
	POODirectory demodir = new POODirectory("demodir");
	System.out.println("System: Adding OOP into directory");
	demodir.add(oopboard);
	System.out.println("System: Add a split into directory");
	demodir.add_split();
	System.out.println("System: Add a new dir into this directory...");
	POODirectory newdir = new POODirectory("newdir");
	demodir.add(newdir);
        System.out.print("System: Press Enter to show() the directory");  
	scanner.nextLine();
        demodir.show();
	System.out.print("System: Press Enter to continue");  
	scanner.nextLine();
	
        System.out.println("System: create some test boards and dirs and add...");  
	POOBoard test1 = new POOBoard("test1");
	POOBoard test2 = new POOBoard("test2");
	POOBoard test3 = new POOBoard("test3");
	POODirectory dirtest1 = new POODirectory("dirtest1");
	POODirectory dirtest2 = new POODirectory("dirtest2");
	POODirectory dirtest3 = new POODirectory("dirtest3");
	demodir.add(test1);
	demodir.add(test2);
	demodir.add(test3);
	demodir.add(dirtest1);
	demodir.add(dirtest2);
	demodir.add(dirtest3);	
        System.out.print("System: Press Enter to show() the directory");  
	scanner.nextLine();
        demodir.show();
	System.out.print("System: Press Enter to continue");  
	scanner.nextLine();
	
        System.out.println("System: move(4,15) the directory");   	
        demodir.move(4,15);     
        System.out.print("System: Press Enter to show() the directory");
	scanner.nextLine();
        demodir.show();     
	System.out.print("System: Press Enter to continue");  
	scanner.nextLine();	
	
        System.out.println("System: del(5) the directory");   	
        demodir.del(5);     
        System.out.print("System: Press Enter to show() the directory");   
	scanner.nextLine();
        demodir.show();     
	System.out.print("System: Press Enter to continue");  
	scanner.nextLine();	
        System.out.println("System: output the length of the directory");  
	System.out.printf("the length is : %d\n", demodir.length());
	System.out.print("System: Demo end. Press Enter to exit.");  
	scanner.nextLine();	
	System.exit(0); 
    }
}
