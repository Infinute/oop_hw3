
public class POODirectory {
    private String name;
    private POOBoard[] board;
    private POODirectory[] dir;
    private int[] map;
    private int length;
    private int tail;
    public POODirectory(String name){
	//create a directory with the name
	this.name = name;
	board = new POOBoard[1024];
	dir = new POODirectory[1024];
	map = new int[1024];
	length = 0;
	tail = 0;
    }
    public void add(POOBoard board){
	if (tail < 1024){
		map[tail]=1;
		this.board[tail]=board;
		length++;
		tail++;
	} else {
		int i=-1;
		if (length<1024){
			i = 1024;
			while(map[--i]>0)
				continue;
			length++;
		}
		while(++i<1023){
			map[i]
				= map[i+1];
			this.board[i]
				= this.board[i+1]; 
		}
		map[1023]=1;
		this.board[1023] = board;
	}
	//append the board to the directory
    }
    public void add(POODirectory dir){
	if (tail < 1024){
		map[tail]=2;
		this.dir[tail]=dir;
		length++;
		tail++;
	} else {
		int i=-1;
		if (length<1024){
			i = 1024;
			while(map[--i]>0)
				continue;
			length++;
		}
		while(++i<1023){
			map[i]
				= map[i+1];
			this.dir[i]
				= this.dir[i+1]; 
		}
		map[1023]=2;
		this.dir[1023] = dir;
	}
	//append the directory to the directory
    }
    public void add_split(){
	if (tail < 1024){
		map[tail]=3;
		length++;
		tail++;
	} else {
		int i=-1;
		if (length<1024){
			i = 1024;
			while(map[--i]>0)
				continue;
			length++;
		}
		while(++i<1023)
			map[i] = map[i+1];
		map[1023]=3;
	}
	//append a splitting line to the directory
    }
    public void del(int pos){
	if (map[pos]==1)
		board[pos]= null;
	else if (map[pos]==2)
		dir[pos] = null;
	map[pos]=0;
	length--;
	if (pos == tail-1)
		pos--;
	//delete the board/directory/splitting line at position pos
    }
    public void move(int src, int dest){
	map[dest]=map[src];
	if (map[src]==1){
		board[dest] = board[src];
		board[src]= null;
	} else if (map[src]==2){
		dir[dest] = dir[src];
		dir[src] = null;
	}
	map[src] = 0;
	//move the board/directory/splitting line at position src to position dest
    }
    public int length(){
	return length;
	//get the current number of items in the directory
    }
    public void show(){
	int i=-1;
	while(++i<1024)
		if (map[i]==1)
			System.out.printf("%3d   %s\n",i,board[i].name());
		else if (map[i]==2)
			System.out.printf("%3d   %s\n",i,dir[i].name());
		else if (map[i]==3)
			System.out.printf("%3d   --------------------\n",i);
	//show the board/directory titles and splitting lines of the directory
    }
    public String name(){
    	return name;
    }
}
