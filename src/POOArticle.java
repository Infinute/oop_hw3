//import java.util.Scanner;

public class POOArticle {
    private static final int MAXEVAL = 500;
    private int ID;
    private String title;
    private String author;
    private String content;
    private int evaluation_count;
    private String[] evaluation_message;
    private int total_evaluation;
    public POOArticle(int ID, String title, String author, String content){
	if (ID > 999)
		System.out.printf("Warning: ID overflows. (ID > 999)\n");
	this.ID = ID;
	this.title = title;
	this.author = author;
	this.content = content;
	evaluation_count = 0;
	total_evaluation = 0;
	evaluation_message = new String[500];
    }
    public int push(String evaluation_message){
	if (total_evaluation < MAXEVAL){
	    ++evaluation_count;
	    this.evaluation_message[total_evaluation] = evaluation_message;
	    ++total_evaluation;
	    return 0;
	} else
	    return -1;
    }
    public int boo(String evaluation_message){
	if (total_evaluation < MAXEVAL){
	    --evaluation_count;
	    this.evaluation_message[total_evaluation] = evaluation_message;
	    ++total_evaluation;
	    return 0;
	} else
	    return -1;
    }   
    public int arrow(String evaluation_message){
	if (total_evaluation < MAXEVAL){
	    this.evaluation_message[total_evaluation] = evaluation_message;
	    ++total_evaluation;
	    return 0;
	} else
	    return -1;
    }
    public void show(){
	System.out.printf("ID: %d\nEvaluation Count: %d\nAuthor: %s\nTitle: %s\nContent:\n%s\n",
						ID, evaluation_count, author, title, content);
	System.out.printf("Evaluation message:\n");
	int i=-1;
	while (++i<total_evaluation){
	    System.out.printf("%s\n", evaluation_message[i]);
	}
    }
    public void list(){
	System.out.printf("ID: %d\nEvaluation Count: %d\nAuthor: %s\nTitle: %s\n",
						ID, evaluation_count, author, title);
    }
    public String title(){
	    return title;
    }
}
